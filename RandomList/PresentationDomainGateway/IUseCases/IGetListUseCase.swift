import Foundation

protocol IGetListUseCase {
    
    func exec(request: DomainDTOs.GetList.Request, completion: @escaping (DomainDTOs.GetList.Response) -> Void)
    
}
