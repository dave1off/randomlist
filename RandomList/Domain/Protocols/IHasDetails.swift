import Foundation

protocol IHasDetails {
    
    var details: String { get }
    
}
