import Foundation

protocol IHasTitle {
    
    var title: String { get }
    
}
