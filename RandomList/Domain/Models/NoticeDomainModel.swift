import Foundation

struct NoticeDomainModel: IHasTitle, IHasDetails {
    
    var flightDate: Date?
    var gate: String?
    
    let title = "Notice"
    
    var details: String {
        return "Flight \(flightDate ?? Date()), gate \(gate ?? "Unknown")"
    }
    
}

