import Foundation

final class MoveDomainModel: IHasTitle, IHasDetails {
    
    var fromPlace: String?
    var toPlace: String?
    var estimateTime: TimeInterval?
    
    let title = "Move"
    
    var details: String {
        return "Move from \(fromPlace ?? "") to \(toPlace ?? ""), estimated time is \(estimateTime ?? 0)"
    }
    
}

