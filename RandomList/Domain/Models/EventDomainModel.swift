import Foundation

final class EventDomainModel: IHasTitle, IHasDetails {
    
    var startTime: Date?
    var endTime: Date?
    var name: String!
    
    let title = "Event"
    
    var details: String {
        return "Event \(name ?? "") lasts from \(startTime ?? Date()) to \(endTime ?? Date())"
    }
    
}
