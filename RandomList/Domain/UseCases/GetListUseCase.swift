import Foundation

final class GetListUseCase: IGetListUseCase {
    
    func exec(request: DomainDTOs.GetList.Request, completion: @escaping (DomainDTOs.GetList.Response) -> Void) {
        DispatchQueue.global().async {
            let total = Int.random(in: 10...100)
            
            var list: [IHasDetails & IHasTitle] = []
            
            for i in 1...total {
                let random = Int.random(in: 1...3)
                
                switch random {
                case 1:
                    list.append(NoticeDomainModel(flightDate: Date(), gate: "Gate \(i)"))
                case 2:
                    let event = EventDomainModel()
                    event.name = "Name \(i)"
                    list.append(event)
                case 3:
                    let move = MoveDomainModel()
                    move.estimateTime = Double(i)
                    list.append(move)
                default:
                    break
                }
            }
            
            completion(DomainDTOs.GetList.Response(items: list.map { DomainDTOs.GetList.Response.Item(title: $0.title, details: $0.details) }))
        }
    }
    
}
