import UIKit


extension UIView {
    
    static var reuseIdentifier: String {
        return description().components(separatedBy: ".").last ?? ""
    }
    
}
