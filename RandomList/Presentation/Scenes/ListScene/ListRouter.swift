import UIKit

protocol IListRouter {
    
    func goTo(vc: UIViewController)
    
}

final class ListRouter: IListRouter {
    
    private weak var view: UIViewController?
    
    init(view: UIViewController) {
        self.view = view
    }
    
    func goTo(vc: UIViewController) {
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
}
