import UIKit

final class ListAssembly: IAssembly {
    
    func build() -> UIViewController {
        let view = ListView()
        let presenter = ListPresenter(getListUseCase: GetListUseCase(), view: view)
        let router = ListRouter(view: view)
        
        view.presenter = presenter
        view.router = router
        
        return view
    }
    
}
