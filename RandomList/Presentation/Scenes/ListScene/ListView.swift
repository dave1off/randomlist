import UIKit

protocol IListView: class {
    
    func getList(response: ListDTOs.ViewDidLoad.Response)
    func didSelectItem(response: ListDTOs.DidSelectItem.Response)
    
}

final class ListView: UIViewController {
    
    private let tableView = UITableView()
    
    var presenter: IListPresenter!
    var router: IListRouter!
    
    private var elements: [ListDTOs.ViewDidLoad.Response.Item] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        presenter.onViewDidLoad(request: .init())
    }
    
}

extension ListView: IListView {
    
    func getList(response: ListDTOs.ViewDidLoad.Response) {
        elements = response.items
    }
    
    func didSelectItem(response: ListDTOs.DidSelectItem.Response) {
        router.goTo(vc: response.assembly.build())
    }
    
}

extension ListView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.onDidSelectItem(request: .init(index: indexPath.row))
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension ListView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.reuseIdentifier, for: indexPath)
        let element = elements[indexPath.row]
        
        cell.textLabel?.text = "\(element.title) - \(element.details)"
        
        return cell
    }
    
}

private extension ListView {
    
    func setupUI() {
        view.backgroundColor = .white
        
        navigationItem.title = "Список"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorInset = .zero
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.reuseIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        ])
    }
    
}
