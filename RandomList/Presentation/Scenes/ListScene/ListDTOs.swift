import Foundation

struct ListDTOs {
    
    struct ViewDidLoad {
        
        struct Request { }
        
        struct Response {
            
            struct Item {
                
                let title: String
                let details: String
                
            }
            
            let items: [Item]
            
        }
        
    }
    
    struct DidSelectItem {
        
        struct Request {
            
            let index: Int
            
        }
        
        struct Response {
            
            let assembly: IAssembly
            
        }
        
    }
    
}
