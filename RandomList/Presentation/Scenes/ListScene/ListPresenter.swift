import Foundation

protocol IListPresenter {
    
    func onViewDidLoad(request: ListDTOs.ViewDidLoad.Request)
    func onDidSelectItem(request: ListDTOs.DidSelectItem.Request)
    
}

final class ListPresenter: IListPresenter {
    
    private let getListUseCase: IGetListUseCase
    
    private weak var view: IListView?
    
    private var list: [DomainDTOs.GetList.Response.Item] = []
    
    init(getListUseCase: IGetListUseCase, view: IListView?) {
        self.getListUseCase = getListUseCase
        self.view = view
    }
    
    func onViewDidLoad(request: ListDTOs.ViewDidLoad.Request) {
        getListUseCase.exec(request: .init()) { [weak self] response in
            guard let self = self else { return }
            
            self.list = response.items
            
            DispatchQueue.main.async {
                self.view?.getList(response: .init(items: response.items.map { ListDTOs.ViewDidLoad.Response.Item(title: $0.title, details: "Детали") }))
            }
        }
    }
    
    func onDidSelectItem(request: ListDTOs.DidSelectItem.Request) {
        let item = list[request.index]
        
        view?.didSelectItem(response: .init(assembly: DetailsAssembly(dto: .init(details: item.details))))
    }
    
}
