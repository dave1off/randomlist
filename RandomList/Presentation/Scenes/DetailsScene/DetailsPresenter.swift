import Foundation

protocol IDetailsPresenter {
    
    func onViewDidLoad(request: DetailsDTOs.ViewDidLoad.Request)
    
}

final class DetailsPresenter: IDetailsPresenter {
    
    private let dto: DetailsDTOs.Init
    
    private weak var view: IDetailsView?
    
    init(dto: DetailsDTOs.Init, view: IDetailsView?) {
        self.dto = dto
        self.view = view
    }
    
    func onViewDidLoad(request: DetailsDTOs.ViewDidLoad.Request) {
        view?.viewDidLoad(response: .init(details: dto.details))
    }
    
}
