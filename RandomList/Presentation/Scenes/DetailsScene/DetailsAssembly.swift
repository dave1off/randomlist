import UIKit

final class DetailsAssembly: IAssembly {
    
    private let dto: DetailsDTOs.Init
    
    init(dto: DetailsDTOs.Init) {
        self.dto = dto
    }
    
    func build() -> UIViewController {
        let view = DetailsView()
        let presenter = DetailsPresenter(dto: dto, view: view)
        
        view.presenter = presenter
        
        return view
    }
    
}
