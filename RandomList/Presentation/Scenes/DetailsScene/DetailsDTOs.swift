import Foundation

struct DetailsDTOs {
    
    struct Init {
        
        let details: String
        
    }
    
    struct ViewDidLoad {
        
        struct Request { }
        
        struct Response {
            
            let details: String
            
        }
        
    }
    
}
