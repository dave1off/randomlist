import UIKit

protocol IDetailsView: class {
    
    func viewDidLoad(response: DetailsDTOs.ViewDidLoad.Response)
    
}

final class DetailsView: UIViewController {
    
    var presenter: IDetailsPresenter!
    
    private let textView = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        presenter.onViewDidLoad(request: .init())
    }
    
}

extension DetailsView: IDetailsView {
    
    func viewDidLoad(response: DetailsDTOs.ViewDidLoad.Response) {
        textView.text = response.details
    }
    
}

private extension DetailsView {
    
    func setupUI() {
        view.backgroundColor = .white
        
        navigationItem.title = "Детали"
        
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = .systemFont(ofSize: 20)
        view.addSubview(textView)
        
        NSLayoutConstraint.activate([
            textView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            textView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            textView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            textView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20)
        ])
    }
    
}
