import UIKit

final class MainNavigationAssembly: IAssembly {
    
    func build() -> UIViewController {
        let view = MainNavigationView()
        let presenter = MainNavigationPresenter(view: view)
        
        view.presenter = presenter
        
        return view
    }
    
}
