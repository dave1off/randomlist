import UIKit

protocol IMainNavigationView: class {
    
    func viewDidLoad(response: MainNavigationDTOs.ViewDidLoad.Response)
    
}

final class MainNavigationView: UINavigationController {
    
    var presenter: IMainNavigationPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.onViewDidLoad(request: .init())
    }
    
}

extension MainNavigationView: IMainNavigationView {
    
    func viewDidLoad(response: MainNavigationDTOs.ViewDidLoad.Response) {
        setViewControllers(response.assemblies.map { $0.build() }, animated: false)
    }
    
}

