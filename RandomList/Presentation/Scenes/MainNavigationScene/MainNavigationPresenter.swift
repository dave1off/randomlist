import Foundation

protocol IMainNavigationPresenter {
    
    func onViewDidLoad(request: MainNavigationDTOs.ViewDidLoad.Request)
    
}

final class MainNavigationPresenter: IMainNavigationPresenter {
    
    private weak var view: IMainNavigationView?
    
    init(view: IMainNavigationView?) {
        self.view = view
    }
    
    func onViewDidLoad(request: MainNavigationDTOs.ViewDidLoad.Request) {
        view?.viewDidLoad(response: .init(assemblies: [ListAssembly()]))
    }
    
}
