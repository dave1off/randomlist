import Foundation

struct MainNavigationDTOs {
    
    struct ViewDidLoad {
        
        struct Request { }
        
        struct Response {
            
            let assemblies: [IAssembly]
            
        }
        
    }
    
}
